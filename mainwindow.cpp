#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTextStream>
#include <iostream>
#include <fstream>
#include <string>
#include <AffdexException.h>

#include <QPixmap>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    countTot=0;
    countBeauty=0;
    countUgly=0;
    numberofacquisition=0;
    startclicked=0;
    countstartclicked=0;
    stopclicked=0;
    currentAcquisition=0;
    beautyLabel=0;
    uglyLabel=0;
    sensitivity=0;


    ui->setupUi(this);
    QPixmap pix("C:/Users/marco/Desktop/imtest/IMG_0561.JPG");
    ui -> label_pic-> setPixmap(pix);
    x=3000;
    timer= new QTimer(this);
    timerAlert=new QTimer(this);
    endAcqTime=new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(change_img()));
    connect(endAcqTime,SIGNAL(timeout()),this,SLOT(endAcq()));
    connect(timerAlert,SIGNAL(timeout()),this,SLOT(acquisition_text()));
    detector.setCameraId(0);
    detector.setCameraFPS(30);

    listenFace=std::shared_ptr<affdex::FaceListener>(new listenerFace());
    detector.setFaceListener(listenFace.get());

    listenImage=std::shared_ptr<affdex::ImageListener>(new listenerImage());

    detector.setImageListener(listenImage.get());
    //ora prima il processo feed che mi permette di mappare le emozioni
    //
    detector.setClassifierPath("/home/marco/Documents/imv/imviewer/Affectiva/data");
    detector.setDetectAllEmotions(true);
    detector.setDetectJoy(true);
    detector.setDetectSmirk(true);
    detector.setDetectSmile(true);
    detector.setDetectEyeWiden(true);

    QDir *curtPath=new QDir(QDir::currentPath().append("/im_proj"));
        MiniImageFiles=curtPath->path();
        QStringList sequence_len=curtPath->entryList();
        sequence_len.removeFirst();
        sequence_len.removeFirst();
        sequence_len.insert(0,"");
        ui->ImageBrowse->addItems(sequence_len);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::endAcq()
{
    endAcqTime->stop();
    writeFileAcquisition();
}

void MainWindow::on_pushButton_clicked()

{

    QFileDialog dialog(this);
    dialog.setNameFilter(tr("Images (*.png *.xpm *.jpg)"));
    dialog.setViewMode(QFileDialog::Detail);
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Images"),"C:/Users/marco/Documents/QTRepo", tr("Images Files(*.png *.jpg)"));

    if(!fileName.isEmpty())
    {
        QImage image(fileName);
        ui-> label_pic->setPixmap(QPixmap::fromImage(image));
    }




}

void MainWindow::on_Start_clicked()
{
    if(currentAcquisition==1)
    {
        timerAlert->start(2000);
        ui->alarmBox->setText("NON E' POSSIBILE PREMERE START DURANTE L'ACQUISIZIONE.");
    }
    else
    {
        currentAcquisition=1;


        numberofacquisition+=1;
        startclicked=1;
        countstartclicked+=1;
        detector.start();



        acquisition_text();


        std::ofstream myfile;
        myfile.open ("example.txt", std::ios::out);
        myfile << "*************  ACQUISITION ************* " <<"\n";
        myfile.close();


        fillimg(probability);//questa è la funzione che fa tutto basta che setti la probabilita da 1 a 100 (se settata a 50 prende le immagini da Image e le prende tutte)
        timer->start(x);
    }
}







void MainWindow::on_Stop_clicked()
{

  timerAlert->stop();
  currentAcquisition=0;
  stopclicked=1;

  writeFileAcquisition();

}








void MainWindow::change_img()
{
  QString loadImage=FilePath;
   int i=0;
  if(ListFiles.size()>1)
  {
  i=qrand()%(ListFiles.size()-1);
  }
    else
    {
      i=0;
      timer->stop();

    }





  loadImage.append("/"+ListFiles.at(i));
  ListFiles.removeAt(i);
  qDebug(" filename:%s,i=%d,List=%d",loadImage.toUtf8().constData(),i,ListFiles.size());
  Stringa=(loadImage);
  std::ofstream myfile;
  myfile.open ("example.txt", std::ios::app);
  myfile << "NOMEFILE: " <<Stringa.toUtf8().constData()<<"\n";
  myfile.close();
  QImage image(loadImage);
  QPixmap imageScaled;
  ui->label_pic->setAlignment(Qt::AlignCenter);
  QImage image2= image.scaled(ui->label_pic->size(),Qt::KeepAspectRatio);
  ui-> label_pic->setPixmap(imageScaled.fromImage(image2));

  if (ListFiles.size()==0)
  {

    timerAlert->stop();
    currentAcquisition=0;

      //2secondi dopo
    endAcqTime->start(x);


  }
 // if (timer->remainingTime()<x/3)
 //{ detector.stop();}

}


void MainWindow::on_chtime_editingFinished()
{

}


void MainWindow::on_chtime_valueChanged(const QString &arg1)
{

}

void MainWindow::on_chtime_valueChanged(int arg1)
{
    x=1000*arg1; //transform in ms
}
void MainWindow::fillimg(int n)
{
    ListFiles.clear();


    QDir *curtPath2=new  QDir(QDir::currentPath().append(("/Brutti")));
    QDir *curtPath1=new QDir(QDir::currentPath().append(("/Belli")));
    QString FilePath2=curtPath2->path();
    QStringList ListFiles2=curtPath2->entryList();
    QString FilePath1=curtPath1->path();
    QStringList  ListFiles1=curtPath1->entryList();
    ListFiles2.removeFirst();
    ListFiles2.removeFirst();
    ListFiles1.removeFirst();
    ListFiles1.removeFirst();
    int lengthUgly,lengthBeauty,lengthMax,lengthMin;
    if (ListFiles1.size()>ListFiles2.size())
    {
        lengthMax=ListFiles1.size();
        lengthMin=ListFiles2.size();
    }
    else
    {
        lengthMax=ListFiles2.size();
        lengthMin=ListFiles1.size();
    }
    if (n>50)
    {
        lengthBeauty=(lengthMax);
        lengthUgly=(int)(((lengthMax)*(100-n))/100);
    }
   else
    {
      if (n<50)
      {

        n=100-n;
        lengthUgly=lengthMax;
        lengthBeauty=(int)(((lengthMax)*(100-n))/100);
      }
      else
      {
          qDebug("ECCOMI:UGLY:%d,beauty:%d",lengthMin,lengthMin);
          lengthUgly=lengthMin;
          lengthBeauty=lengthMin;
      }
    }
    int i=0;
   for (i=0;i<lengthBeauty;i++)
   {
       QString path=FilePath1;
       int temp=qrand()%(ListFiles1.size());
       ListFiles.insert(i,path.append("/"+ListFiles1.at(temp)));
       ListFiles1.removeAt(temp);
       qDebug("gauss:%s",ListFiles.at(i).toUtf8().constData());
   }

    int  j=i;
   for (i=i;i<(lengthUgly+j);i++)
   {
       QString path1=FilePath2;
       int temp=qrand()%(ListFiles2.size());
       ListFiles.insert(i,path1.append("/"+ListFiles2.at(temp)));
       ListFiles2.removeAt(temp);
       qDebug("gauss:%s",ListFiles.at(i).toUtf8().constData());
    }

    qDebug("listfilesize:%d",ListFiles.size());



}

void MainWindow::on_chprobability_editingFinished()
{

}

void MainWindow::on_chprobability_valueChanged(int arg1)
{
    probability=arg1;
}


void MainWindow::acquisition_text()
{
    QString numofacq= QString::number(numberofacquisition);
    QString acqLabel=acquisitionLabel+numofacq;
    ui->alarmBox->setText(acqLabel);
}


void MainWindow::writeFileAcquisition()
{
    if(startclicked==1)
    {

      std::ofstream myfile;
      myfile.open ("example.txt", std::ios::app);
      myfile << "*************  END ACQUISITION ************* " <<"\n";
      myfile.close();
      timer->stop();

      ui->alarmBox->setText("ACQUISIZIONE TERMINATA!");



      int countNecessaryDetectionJoy=1;
      int countInitImageJoy;
      int countNecessaryDetectionSmile=1;
      int countInitImageSmile;
      int countNecessaryDetectionEyeWiden=1;
      int countInitImageEyeWiden;

      int countNecessaryDetectionFear=1;
      int countInitImageFear;
      int countNecessaryDetectionDisgust=1;
      int countInitImageDisgust;

      double tempJoy=0;
      double tempSmile=0;
      double tempEyeWiden=0;

      double tempFear=0;
      double tempDisgust=0;



      std::string line;
      std::string initString;

      std::ofstream myfile3;


      if(countstartclicked==1)
        {
            myfile3.open ("RESULTS.txt", std::ios::out);
        }
      else
        {
            myfile3.open ("RESULTS.txt", std::ios::app);
        }




      std::ifstream myfile2 ("example.txt");
      if (myfile2.is_open())
      {


        while ( getline (myfile2,line) )
        {
          std::string substring =line.substr(26,15);
          std::string initString=line.substr(0,1);
          myfile << "J_EMOTION JOY VALUE:      " << tempJoy<<"\n";
          myfile << "S_EMOTION SMILE VALUE:    " << tempSmile<<"\n";
          myfile << "E_EMOTION EYEWIDEN VALUE: " << tempEyeWiden<<"\n";


          myfile << "F_EMOTION FEAR VALUE:     " << tempFear<<"\n";
          myfile << "D_EMOTION DISGUST VALUE:  " << tempDisgust<<"\n";

          if(initString=="*" || initString=="N")
          {
              tempJoy=tempJoy/countNecessaryDetectionJoy;
              tempSmile=tempSmile/countNecessaryDetectionSmile;
              tempEyeWiden=tempEyeWiden/countNecessaryDetectionEyeWiden;

              tempFear=tempFear/countNecessaryDetectionFear;
              tempDisgust=tempDisgust/countNecessaryDetectionDisgust;

              if(((tempJoy!=0) || (tempSmile!=0)|| (tempEyeWiden!=0)) && ((tempFear!=0)||(tempDisgust!=0))  )
              {
                  if((tempJoy>50) && (tempSmile>50))
                  {
                      myfile3<<"IMMAGINE RICONOSCIUTA COME BELLA"<<"\n";
                  }
                  else
                  {
                      if((tempFear>5) || (tempDisgust>5))
                      {
                      myfile3<<"IMMAGINE RICONOSCIUTA COME BRUTTA"<<"\n";
                      }
                      else
                      {
                          myfile3<<"NON CI SONO ABBASTANZA INFORMAZIONI PER CATALOGARE L'IMMAGINE"<<"\n";
                      }
                  }

              }

              else
              {
                  myfile3<<"NON CI SONO ABBASTANZA INFORMAZIONI PER CATALOGARE L'IMMAGINE"<<"\n";
              }


              if(tempJoy!=0)
              {myfile3<<"VALORE MEDIO GIOIA: "<<tempJoy<<"\n";
              }

              if(tempSmile!=0)
              {myfile3<<"VALORE MEDIO SMILE: "<<tempSmile<<"\n";
              }

              if(tempEyeWiden!=0)
              {myfile3<<"VALORE MEDIO EYEWIDEN: "<<tempEyeWiden<<"\n";
              }


              if(tempFear!=0)
              {myfile3<<"VALORE MEDIO FEAR: "<<tempFear<<"\n";
              }


              if(tempDisgust!=0)
              {myfile3<<"VALORE MEDIO DISGUST: "<<tempDisgust<<"\n";
              }

              myfile3<<line<<"\n";
              countInitImageJoy=0;
              countNecessaryDetectionJoy=0;
              tempJoy=0;

              countInitImageSmile=0;
              countNecessaryDetectionSmile=0;
              tempSmile=0;

              countInitImageEyeWiden=0;
              countNecessaryDetectionEyeWiden=0;
              tempEyeWiden=0;


              countInitImageFear=0;
              countNecessaryDetectionFear=0;
              tempFear=0;


              countInitImageDisgust=0;
              countNecessaryDetectionDisgust=0;
              tempDisgust=0;
          }

          else
          {
           if(initString=="J")
              {
                  countInitImageJoy+=1;
                  if(countInitImageJoy>5)
                      //vuol dire che dopo 5 acquisizioni inizio a dare rilevanza all'emozione rilevata(orientativamente ci vogliono 5 rilevamenti tra un'immagine e l'altra
                  {
                  countNecessaryDetectionJoy+=1;
                  tempJoy+=::atof(substring.c_str());
                  }
              }
           if(initString=="S")
              {
                  countInitImageSmile+=1;
                  if(countInitImageSmile>5)
                      //vuol dire che dopo 5 acquisizioni inizio a dare rilevanza all'emozione rilevata(orientativamente ci vogliono 5 rilevamenti tra un'immagine e l'altra
                  {
                  countNecessaryDetectionSmile+=1;
                  tempSmile+=::atof(substring.c_str());
                  }
              }

           if(initString=="E")
              {
                  countInitImageEyeWiden+=1;
                  if(countInitImageEyeWiden>5)
                      //vuol dire che dopo 5 acquisizioni inizio a dare rilevanza all'emozione rilevata(orientativamente ci vogliono 5 rilevamenti tra un'immagine e l'altra
                  {
                  countNecessaryDetectionEyeWiden+=1;
                  tempEyeWiden+=::atof(substring.c_str());
                  }
              }


           if(initString=="F")
              {
                  countInitImageFear+=1;
                  if(countInitImageFear>5)
                      //vuol dire che dopo 5 acquisizioni inizio a dare rilevanza all'emozione rilevata(orientativamente ci vogliono 5 rilevamenti tra un'immagine e l'altra
                  {
                  countNecessaryDetectionFear+=1;
                  tempFear+=::atof(substring.c_str());
                  }
              }


           if(initString=="D")
              {
                  countInitImageDisgust+=1;
                  if(countInitImageDisgust>5)
                      //vuol dire che dopo 5 acquisizioni inizio a dare rilevanza all'emozione rilevata(orientativamente ci vogliono 5 rilevamenti tra un'immagine e l'altra
                  {
                  countNecessaryDetectionDisgust+=1;
                  tempDisgust+=::atof(substring.c_str());
                  }
              }



          }


        }
       myfile2.close();



      }

      else std::cout << "Unable to open file";

      myfile3.close();

      startclicked=0;


    }
    else
    {
         ui->alarmBox->setText("NON HAI ANCORA PREMUTO START");
    }
}

void MainWindow::on_ImageBrowse_activated(const QString &arg1)
{

    countFileAcquisition(arg1.toStdString());



    if(beautyLabel==1)
    {
       if((countBeauty+countUgly)!=0)
       {sensitivity=countBeauty*100/(countBeauty+countUgly);
        }
       ui->Expected->setText("BEAUTY");

    }

    else
       if(uglyLabel==1)
        {
           ui->Expected->setText("UGLY");

           if((countBeauty+countUgly)!=0)
           {  sensitivity=countUgly*100/(countUgly+countBeauty);
            }
        }
       else
       {
           sensitivity=0;
       }


    ui->Sensitivity->setText(QString::number(sensitivity));
    ui->Counts->setText(QString::number(countTot));
    ui->Beauty->setText(QString::number(countBeauty));
    ui->Ugly->setText(QString::number(countUgly));

 QString Image=MiniImageFiles;
     Image.append("/"+arg1);
  QImage image(Image);
  QPixmap imageScaled;
    ui->MiniImage->setAlignment(Qt::AlignCenter);
    QImage image2= image.scaled(ui->MiniImage->size(),Qt::KeepAspectRatio);
    ui-> MiniImage->setPixmap(imageScaled.fromImage(image2));

    countTot=0;
    countBeauty=0;
    countUgly=0;
}




void MainWindow::countFileAcquisition(std::string fileName)
{

      beautyLabel=0;
      uglyLabel=0;

      std::string line;
      std::string initString;





      std::ifstream myfile3 ("RESULTS.txt");
      if (myfile3.is_open())
      {


        while ( getline (myfile3,line) )
        {
          std::string initString=line.substr(0,8);


          if(initString=="NOMEFILE")
          {

              std::size_t pos=line.find(fileName);
              if(pos!= -1)
              {
                  std::size_t pos4=line.find("Belli");
                  if (pos4!=-1)
                  {
                    beautyLabel=1;
                  }
                  else
                  {
                    uglyLabel=1;
                  }

                  countTot+=1;
                  getline(myfile3,line);
                  std::size_t pos1=line.find("BELLA");
                  if(pos1!=-1)
                  {
                      countBeauty+=1;
                  }
                  else
                  {
                      std::size_t pos2=line.find("BRUTTA");
                      if(pos2!=-1)
                      {
                          countUgly+=1;
                      }
                  }
              }
           }
        }
      }

      myfile3.close();





}


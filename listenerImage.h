#ifndef LISTENERIMAGE_H
#define LISTENERIMAGE_H

#include <CameraDetector.h>
#include <fstream>
#include <iostream>
#include <listenerFace.h>
#include <listenerImage.h>
#include <string.h>
#include <QString>

class listenerImage : public affdex::ImageListener
{
private:
    QString imgname;

public:
    listenerImage();
    void onImageResults(std::map<affdex::FaceId,affdex::Face> faces, affdex::Frame image);
    void onImageCapture (affdex::Frame image);

};

#endif // LISTENERIMAGE_H

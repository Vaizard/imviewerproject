#ifndef LISTENER_H
#define LISTENER_H



#include <CameraDetector.h>
#include <fstream>
#include <iostream>



class listenerFace: public affdex::FaceListener
{
public:
    listenerFace();
    void onFaceFound(float timestamp,affdex::FaceId faceId);
    void onFaceLost(float timestamp,affdex::FaceId faceId);
};

#endif // LISTENER_H

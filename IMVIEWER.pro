#-------------------------------------------------
#
# Project created by QtCreator 2017-10-21T16:56:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = IMVIEWER
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    listenerFace.cpp \
    listenerImage.cpp


HEADERS  += mainwindow.h \
    listenerFace.h \
    listenerImage.h


FORMS    += mainwindow.ui


INCLUDEPATH += $$PWD/Affectiva/include/
DEPENDPATH += $$PWD/Affectiva/include/




unix:!macx: LIBS += -L$$PWD/Affectiva/lib/ -laffdex-native
unix:!macx: LIBS += -L$$PWD/Affectiva/lib/ -lavcodec
unix:!macx: LIBS += -L$$PWD/Affectiva/lib/ -lavdevice
unix:!macx: LIBS += -L$$PWD/Affectiva/lib/ -lavfilter
unix:!macx: LIBS += -L$$PWD/Affectiva/lib/ -lavformat
unix:!macx: LIBS += -L$$PWD/Affectiva/lib/ -lavutil
unix:!macx: LIBS += -L$$PWD/Affectiva/lib/ -lswresample
unix:!macx: LIBS += -L$$PWD/Affectiva/lib/ -lswscale
unix:!macx: LIBS += -L$$PWD/Affectiva/lib/ -lavutil
unix:!macx: LIBS += -L$$PWD/Affectiva/lib/ -ltensorflow

#include "mainwindow.h"
#include <QApplication>
#include <iostream>
#include <CameraDetector.h>
#include <fstream>
#include <QStyle>
#include <QDesktopWidget>
#include <listenerFace.h>
#include <listenerImage.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setGeometry(QStyle::alignedRect(
                      Qt::LeftToRight,
                      Qt::AlignCenter,
                      w.size(),
                      qApp->desktop()->availableGeometry()));
    w.show();
    return a.exec();
}


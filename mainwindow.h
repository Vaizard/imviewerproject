#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QLabel>
#include <QTimer>
#include <QDir>
#include <iostream>
#include <time.h>
#include <CameraDetector.h>
#include <fstream>
#include <listenerFace.h>
#include <listenerImage.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void change_img();
    void endAcq();

    void on_Start_clicked();

    void on_chtime_editingFinished();

    void on_chtime_valueChanged(const QString &arg1);

    void on_chtime_valueChanged(int arg1);

    void on_chprobability_editingFinished();

    void on_chprobability_valueChanged(int arg1);

    void on_Stop_clicked();

    void acquisition_text();

    void on_ImageBrowse_activated(const QString &arg1);

private:

    void writeFileAcquisition();

    void countFileAcquisition(std::string fileName);

    void fillimg(int n);
        QString Stringa;
        affdex::CameraDetector detector;
        std::shared_ptr<affdex::FaceListener> listenFace ;
        std::shared_ptr<affdex::ImageListener> listenImage;
        Ui::MainWindow *ui;
        QTimer *timer;
        QTimer *endAcqTime;
        QString FilePath;

        int x;
        int countstartstop;
        int probability;

        int countstartclicked;
        bool startclicked;
        bool stopclicked;
        bool currentAcquisition;

        QString acquisitionLabel=" ACQUISIZIONE IN CORSO N. ";
        int numberofacquisition;

        QTimer *timerAlert;


        QString MiniImageFiles;
        QStringList ListFiles;

        int countTot;
        int countBeauty;
        int countUgly;

        bool uglyLabel;
        bool beautyLabel;
        float sensitivity;


};

#endif // MAINWINDOW_H
